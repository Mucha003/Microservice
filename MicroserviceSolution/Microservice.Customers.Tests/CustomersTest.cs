using Microservice.Customers.Api.BL;
using Microservice.Customers.Api.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Microservice.Customers.Tests
{
    [TestClass]
    public class CustomersTest
    {
        [TestMethod]
        public void GetAsyncReturnsOk()
        {
            var customer = new CustomerProvider();
            var customerController = new CustomersController(customer);

            var result = customerController.GtAsync("1").Result;
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
        }



        [TestMethod]
        public void GetAsyncReturnsNotFound()
        {
            var customer = new CustomerProvider();
            var customerController = new CustomersController(customer);

            var result = customerController.GtAsync("123").Result;
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

    }
}
