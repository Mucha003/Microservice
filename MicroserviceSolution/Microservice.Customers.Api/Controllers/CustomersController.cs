﻿using Microservice.Customers.Api.BL;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Microservice.Customers.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerProvider _repos;
        public CustomersController(ICustomerProvider repos)
        {
            _repos = repos;
        }


        [HttpGet("{Id}")]
        public async Task<IActionResult> GtAsync(string Id)
        {
            Models.Customer result = await _repos.GetAsync(Id);
            if (result != null)
            {
                return Ok(result);
            }

            return NotFound();
        }

    }
}
