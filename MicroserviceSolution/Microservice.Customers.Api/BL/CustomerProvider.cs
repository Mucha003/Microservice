﻿using Microservice.Customers.Api.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microservice.Customers.Api.BL
{
    public class CustomerProvider : ICustomerProvider
    {
        private readonly List<Customer> _repos = new List<Customer>();

        public CustomerProvider()
        {
            _repos.Add(new Customer() { Id = "1", Name = "Michael", City = "Bruxelles" });
            _repos.Add(new Customer() { Id = "2", Name = "Benavides", City = "St-Guilles" });
            _repos.Add(new Customer() { Id = "3", Name = "Bruce", City = "Gotham" });
            _repos.Add(new Customer() { Id = "4", Name = "Ethan", City = "London" });
        }

        public Task<Customer> GetAsync(string Id)
        {
            return Task.FromResult(_repos.FirstOrDefault(x => x.Id == Id));
        }
    }
}
