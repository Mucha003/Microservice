﻿using Microservice.Customers.Api.Models;
using System.Threading.Tasks;

namespace Microservice.Customers.Api.BL
{
    public interface ICustomerProvider
    {
        Task<Customer> GetAsync(string Id);
    }

}
