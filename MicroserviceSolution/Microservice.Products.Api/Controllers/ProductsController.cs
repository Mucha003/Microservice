﻿using Microservice.Products.Api.BL;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Microservice.Products.Api.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private IProductProvider _repos;

        public ProductsController(IProductProvider repos)
        {
            _repos = repos;
        }



        [HttpGet("{Id}")]
        public async Task<IActionResult> GtAsync(string Id)
        {
            var result = await _repos.GetAsync(Id);
            if (result != null)
            {
                return Ok(result);
            }

            return NotFound();
        }


    }

}
