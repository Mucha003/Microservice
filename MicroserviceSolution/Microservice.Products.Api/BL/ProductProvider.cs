﻿using Microservice.Products.Api.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microservice.Products.Api.BL
{
    public class ProductProvider : IProductProvider
    {
        private readonly List<Product> _repos = new List<Product>();
        public ProductProvider()
        {
            for (int i = 1; i <= 100; i++)
            {
                _repos.Add(new Product()
                {
                    Id = i.ToString(),
                    Name = $"Product-{i}",
                    Price = i * 3.14
                });
            }
        }


        public Task<Product> GetAsync(string Id)
        {
            return Task.FromResult(_repos.FirstOrDefault(x => x.Id == Id));
        }
    }
}
