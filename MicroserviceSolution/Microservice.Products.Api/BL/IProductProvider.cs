﻿using Microservice.Products.Api.Models;
using System.Threading.Tasks;

namespace Microservice.Products.Api.BL
{
    public interface IProductProvider
    {
        Task<Product> GetAsync(string Id);
    }
}
