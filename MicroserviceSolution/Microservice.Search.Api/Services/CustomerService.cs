﻿using Microservice.Search.Api.Interfaces;
using Microservice.Search.Api.Models;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Microservice.Search.Api.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public CustomerService(IHttpClientFactory HttpClientFactory)
        {
            _httpClientFactory = HttpClientFactory;
        }


        public async Task<Customer> GetAsync(string CustomerId)
        {
            // Appeler le service
            // CreateClient("") => le mm que celui qu'on a mis dans la class StartUp.
            var client = _httpClientFactory.CreateClient("CustomerApi");
            var response = await client.GetAsync($"api/customers/{CustomerId}");

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var customer = JsonConvert.DeserializeObject<Customer>(content);
                return customer;
            }

            return null;
        }
    }
}
