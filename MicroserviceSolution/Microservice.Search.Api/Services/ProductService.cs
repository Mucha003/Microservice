﻿using Microservice.Search.Api.Interfaces;
using Microservice.Search.Api.Models;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace Microservice.Search.Api.Services
{
    public class ProductService : IProductService
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public ProductService(IHttpClientFactory HttpClientFactory)
        {
            _httpClientFactory = HttpClientFactory;
        }


        public async Task<Product> GetAsync(string ProductId)
        {
            // Appeler le service
            // CreateClient("") => le mm que celui qu'on a mis dans la class StartUp.
            HttpClient client = _httpClientFactory.CreateClient("ProductApi");
            HttpResponseMessage response = await client.GetAsync($"api/Products/{ProductId}");

            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                Product product = JsonConvert.DeserializeObject<Product>(content);
                return product;
            }

            return null;
        }
    }
}
