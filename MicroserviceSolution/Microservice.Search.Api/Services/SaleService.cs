﻿using Microservice.Search.Api.Interfaces;
using Microservice.Search.Api.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Microservice.Search.Api.Services
{
    public class SaleService : ISaleService
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public SaleService(IHttpClientFactory HttpClientFactory)
        {
            _httpClientFactory = HttpClientFactory;
        }


        public async Task<ICollection<Order>> GetAsync(string CustomerId)
        {
            // Appeler le service
            // CreateClient("") => le mm que celui qu'on a mis dans la class StartUp.
            HttpClient client = _httpClientFactory.CreateClient("SaleApi");
            HttpResponseMessage response = await client.GetAsync($"api/Sales/{CustomerId}");

            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                ICollection<Order> orders = JsonConvert.DeserializeObject<ICollection<Order>>(content);
                return orders;
            }

            return null;
        }
    }
}
