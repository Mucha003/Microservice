﻿using Microservice.Search.Api.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Microservice.Search.Api.Interfaces
{
    public interface ISaleService
    {
        Task<ICollection<Order>> GetAsync(string CustomerId);
    }
}
