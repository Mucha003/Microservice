﻿using Microservice.Search.Api.Models;
using System.Threading.Tasks;

namespace Microservice.Search.Api.Interfaces
{
    public interface IProductService
    {
        Task<Product> GetAsync(string ProductId);
    }
}
