﻿using Microservice.Search.Api.Models;
using System.Threading.Tasks;

namespace Microservice.Search.Api.Interfaces
{
    // tout ce que nous avons besoins 
    // pour communiquer avec ce service.
    public interface ICustomerService
    {
        // communique avec le api Customer
        Task<Customer> GetAsync(string CustomerId);
    }
}
