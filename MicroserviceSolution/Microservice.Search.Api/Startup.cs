using Microservice.Search.Api.Interfaces;
using Microservice.Search.Api.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using Polly;

namespace Microservice.Search.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<ICustomerService, CustomerService>();
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<ISaleService, SaleService>();

            services.AddControllers();

            //----------    Microservice Externes    ----------
            //On lui passe les services Avec qui on va Communiquer
            services.AddHttpClient("CustomerApi", c =>
            {
                c.BaseAddress = new Uri(Configuration["Api:Customers"]);
            }).AddTransientHttpErrorPolicy(c => c.WaitAndRetryAsync(5, _ => TimeSpan.FromMilliseconds(500)));

            services.AddHttpClient("ProductApi", p =>
            {
                p.BaseAddress = new Uri(Configuration["Api:Products"]);
            });

            services.AddHttpClient("SaleApi", p =>
            {
                p.BaseAddress = new Uri(Configuration["Api:Sales"]);
            });
            //----------    End - Microservice Externes    ----------
        }


        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
