﻿using Microservice.Search.Api.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Microservice.Search.Api.Controllers
{
    [Route("Api/[controller]")]
    [ApiController]
    public class SearchController : ControllerBase
    {
        private readonly ICustomerService _customerService;
        private readonly IProductService _producService;
        private readonly ISaleService _saleService;

        public SearchController(
            ICustomerService CustomerService,
            IProductService ProducService,
            ISaleService SaleService)
        {
            _customerService = CustomerService;
            _producService = ProducService;
            _saleService = SaleService;
        }


        [HttpGet("Customers/{CustomerId}")]
        public async Task<IActionResult> SearchAsync(string CustomerId)
        {
            if (string.IsNullOrWhiteSpace(CustomerId))
            {
                return BadRequest();
            }

            try
            {
                var customer = await _customerService.GetAsync(CustomerId);
                var sales = await _saleService.GetAsync(CustomerId);

                foreach (var sale in sales)
                {
                    foreach (var item in sale.Items)
                    {
                        // /!\ must to return a default value.
                        var product = await _producService.GetAsync(item.ProductId);
                        item.Product = product;
                    }
                }


                var result = new
                {
                    Customer = customer,
                    Sales = sales
                };

                return Ok(result);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
