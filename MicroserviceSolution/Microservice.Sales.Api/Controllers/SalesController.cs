﻿using Microservice.Sales.Api.BL;
using Microservice.Sales.Api.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microservice.Sales.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalesController : ControllerBase
    {
        private readonly ISaleProvider _repos;

        public SalesController(ISaleProvider Repos)
        {
            _repos = Repos;
        }


        [HttpGet("{CustomerId}")]
        public async Task<IActionResult> GetAsync(string CustomerId)
        {
            ICollection<Order> result = await _repos.GetAsync(CustomerId);
            
            if (result != null && result.Any())
            {
                return Ok(result);
            }

            return NotFound();
        }

    }
}
