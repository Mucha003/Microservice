﻿using Microservice.Sales.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microservice.Sales.Api.BL
{
    public class SaleProvider : ISaleProvider
    {
        private readonly List<Order> _repos = new List<Order>();

        public SaleProvider()
        {
            _repos.Add(new Order()
            {
                Id = "0001",
                CustomerId = "1",
                OrderDate = DateTime.Now.AddMonths(-1),
                Total = 100,
                Items = new List<OrderItem>()
                {
                    new OrderItem()
                    {
                        Id = "1",
                        OrderId = "0001",
                        ProductId = "23",
                        Quantity = 2,
                        Price = 100
                    }
                }
            });
            _repos.Add(new Order()
            {
                Id = "0001",
                CustomerId = "1",
                OrderDate = DateTime.Now.AddMonths(-1),
                Total = 100,
                Items = new List<OrderItem>()
                {
                    new OrderItem()
                    {
                        Id = "1",
                        OrderId = "0001",
                        ProductId = "23",
                        Quantity = 2,
                        Price = 100
                    }
                }
            });

            _repos.Add(new Order()
            {
                Id = "0002",
                CustomerId = "2",
                OrderDate = DateTime.Now.AddMonths(-2),
                Total = 100,
                Items = new List<OrderItem>()
                {
                    new OrderItem()
                    {
                        Id = "2",
                        OrderId = "0002",
                        ProductId = "32",
                        Quantity = 22,
                        Price = 200
                    },
                    new OrderItem()
                    {
                        Id = "5",
                        OrderId = "0005",
                        ProductId = "53",
                        Quantity = 5,
                        Price = 56
                    }
                }
            });

            _repos.Add(new Order()
            {
                Id = "0003",
                CustomerId = "3",
                OrderDate = DateTime.Now.AddDays(10),
                Total = 300,
                Items = new List<OrderItem>()
                {
                    new OrderItem()
                    {
                        Id = "3",
                        OrderId = "0003",
                        ProductId = "53",
                        Quantity = 3,
                        Price = 300
                    }
                }
            });

            _repos.Add(new Order()
            {
                Id = "0004",
                CustomerId = "4",
                OrderDate = DateTime.Now.AddMonths(1),
                Total = 400,
                Items = new List<OrderItem>()
                {
                    new OrderItem()
                    {
                        Id = "4",
                        OrderId = "0004",
                        ProductId = "43",
                        Quantity = 34,
                        Price = 400
                    }
                }
            });

        }


        public Task<ICollection<Order>> GetAsync(string CustomerId)
        {
            List<Order> orders = _repos.Where(c => c.CustomerId == CustomerId).ToList();
            return Task.FromResult((ICollection<Order>)orders);
        }

    }
}
