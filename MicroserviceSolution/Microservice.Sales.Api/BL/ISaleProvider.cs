﻿using Microservice.Sales.Api.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Microservice.Sales.Api.BL
{
    public interface ISaleProvider
    {
        Task<ICollection<Order>> GetAsync(string CustomerId);
    }
}
